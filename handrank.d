import std.algorithm.iteration;
import std.array;
import std.conv;
import std.exception;
import std.file;
import std.stdio;
import std.string;

import dice;

void main()
{
	string s;
	uint[2] counts;
	while ((s = readln().strip) !is null)
		if (s.length == 13 && s[0..2] == "> ")
		{
			s = s[2..$];
			ubyte[5] plHand = s[0.. 5].map!(c => cast(ubyte)(c - '1')).array[0..5];
			ubyte[5] aiHand = s[6..11].map!(c => cast(ubyte)(c - '1')).array[0..5];
			auto plRank = diceRanks[encode(plHand)];
			auto aiRank = diceRanks[encode(aiHand)];
			if (plRank != aiRank)
				counts[plRank < aiRank]++;
		}
	writeln(counts[]);
}
