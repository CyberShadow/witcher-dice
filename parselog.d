import std.conv;
import std.file;
import std.stdio;
import std.string;

import ae.utils.array;

import dice;

// Parse output of play.d

void main(string[] args)
{
	auto log = readText(args[1]).splitLines;
	auto games = log.split("##### New game #####");
	uint wins, losses;
	uint[2] rankResults;
	foreach (game; games)
	{
		if (game.indexOf("----------------") >= 0) // exception
			continue;
		wins += game.count("We won!");
		wins += game.count("Opponent passed (we won).");
		losses += game.count("We lost!");

		foreach (line; game)
			if (line.startsWith("Roll "))
			{
				auto roll = line[8..$].to!(ubyte[5][2]);
				auto plRank = diceRanks[encode(roll[0])];
				auto aiRank = diceRanks[encode(roll[1])];
				if (plRank != aiRank)
					rankResults[plRank < aiRank]++;
			}
	}
	writeln("Wins/losses: ", wins, " ", losses);
	writeln("Hand rank results: ", rankResults[]);
}
