import std.algorithm.iteration;
import std.conv;
import std.datetime.systime;
import std.exception;
import std.format;
import std.math;
import std.meta;
import std.process;
import std.range;
import std.stdio;
import std.typecons;

import core.thread;

import ae.sys.sendinput;
import ae.utils.funopt;
import ae.utils.geometry;
import ae.utils.graphics.color;
import ae.utils.graphics.image;
import ae.utils.math;

import dice;

enum screenX0 = 1920;
enum screenY0 = 0;

void waitFor(lazy bool condition, lazy void action, Duration interval = 16.msecs)
{
	Duration totalWaited;
	while (!condition)
	{
		action();
		Thread.sleep(interval);
		totalWaited += interval;
		enforce(totalWaited <= 30.seconds, "Timeout!");
	}
}

void waitUntil(lazy bool condition, lazy void action, Duration interval = 16.msecs)
{
	Duration totalWaited;
	do
	{
		action();
		Thread.sleep(interval);
		totalWaited += interval;
		enforce(totalWaited <= 30.seconds, "Timeout!");
	}
	while (!condition);
}

bool checkForPixel(int x, int y, RGB c)
{
	auto p = getPixel(screenX0 + x, screenY0 + y);
	return p.r == c.r && p.g == c.g && p.b == c.b;
}

void waitForPixel(int x, int y, RGB c)
{
	waitFor(checkForPixel(x, y, c), {}(), 10.msecs);
}

void press(string key)
{
	spawnProcess(["xdotool", "key", key]).wait();
}

void click(string button = "1")
{
	spawnProcess(["xdotool", "mousedown", button]).wait();
	Thread.sleep(16.msecs);
	spawnProcess(["xdotool", "mouseup", button]).wait();
	Thread.sleep(16.msecs);
}

void setMousePos(int x, int y)
{
	ae.sys.sendinput.setMousePos(screenX0+x, screenY0+y);
}

enum dieSize = 25 * 2;

ubyte readDie(int dieX, int dieY, Image!BGRA face)
{
	enum colorBits = 3;
	alias Color = ubyte;
	enum numColors = 1 << colorBits;

	static ubyte luminosity(BGRA c)
	{
		uint sum;
		foreach (ch; only(c.r, c.g, c.b))
			sum += ch;
		sum /= 3;
		return cast(ubyte)sum;
	}

	ubyte maxLum = ubyte.min, minLum = ubyte.max;
	foreach (y; 0..face.w)
		foreach (x; 0..face.h)
		{
			auto lum = luminosity(face[x, y]);
			if (minLum > lum) minLum = lum;
			if (maxLum < lum) maxLum = lum;
		}

	Color convertColor(BGRA c)
	{
		auto lum = luminosity(c);
		auto result = cast(Color)itpl(0, numColors, int(lum), int(minLum), int(maxLum+1));
		assert(result < numColors, format("lum=%d minLum=%d maxLum=%d result=%d", lum, minLum, maxLum, result));
		return result;
	}

	//debug(die_pixels)
	debug
	{
		static Image!BGRA checkFace;
		checkFace.size(face.w, face.h);
		checkFace.pixels[] = BGRA.init;

		scope(failure) checkFace.toBMP.toFile(format("bad-die-pixels-%s.bmp", Clock.currTime.stdTime));
		BGRA facePixel(int x, int y) { return checkFace[x, y] = face[x, y]; }
	}
	else
		BGRA facePixel(int x, int y) { return face[x, y]; }

	enum pipDist = 19;
	auto faceCoords = chain(
		chain(
			only(-pipDist/2, pipDist/2).map!(x =>
				only(-1, 0, 1).map!(dx =>
					iota(-pipDist, pipDist + 1).map!(y =>
						tuple(x + dx, y)
					)
				).joiner
			).joiner
		).map!(t => only(t, tuple(t[1], t[0]))).joiner,
		only(-pipDist, pipDist).map!(px =>
			iota(-3, 3+1).map!(x =>
				iota(-3, 3+1).map!(y =>
					tuple(px + x, y)
				)
			).joiner
		).joiner
	);

	uint[numColors] colors;
	foreach (x, y; faceCoords)
	{
		auto c = convertColor(facePixel(dieSize/2 + x, dieSize/2 + y));
		auto num = ++colors[c];
		// if (medianColorCount < num)
		// {
		// 	medianColor = c;
		// 	medianColorCount = num;
		// }
	}

	enum blurTimes = 1;
	foreach (blurTime; 0..blurTimes)
	{
		uint[numColors] colors2;
		foreach (ushort c; 0..numColors)
		{
			uint numHits, numSamples;
			numHits += colors[c]; numSamples++;

			if (c > 0)
			{
				numHits += colors[c-1];
				numSamples++;
			}
			if (c < numColors-1)
			{
				numHits += colors[c+1];
				numSamples++;
			}

			colors2[c] = numHits / numSamples;
		}
		colors = colors2;
	}

	//auto medianColorChans = explodeColor(medianColor);

	auto numFaceCoords = faceCoords.walkLength;

	version(unittest)
		foreach (c; 0 .. numColors)
		{
			writef("%1.5f", float(colors[c]) / numFaceCoords);
			if ((c & ((1 << colorBits) - 1)) == ((1 << colorBits) - 1))
				writeln;
			else
				write(" ");
		}

	Color maxColor; uint maxSamples;
	foreach (Color c, samples; colors)
		if (maxSamples < samples)
		{
			maxSamples = samples;
			maxColor = c;
		}
	version(unittest) writefln("Max color: %d (%d samples)", maxColor, maxSamples);

	uint pips;
	foreach (pip; 0 .. 3*3)
	{
		auto px = pip % 3 - 1;
		auto py = pip / 3 - 1;
		auto pipX = pipDist * px;
		auto pipY = pipDist * py;
		uint r, g, b;
		enum sampleW = 1;
		foreach (dx; -sampleW .. sampleW+1)
			foreach (dy; -sampleW .. sampleW+1)
			{
				auto c = facePixel(dieSize/2 + pipX + dx, dieSize/2 + pipY + dy);
				r += c.r;
				g += c.g;
				b += c.b;
			}
		enum numSamples = sqr(sampleW * 2 + 1);
		auto avgColor = BGRA(cast(ubyte)(b / numSamples), cast(ubyte)(g / numSamples), cast(ubyte)(r / numSamples));
		auto pipColor = convertColor(avgColor);
		// auto chans = explodeColor(pipColor);
		// auto diff = chans;
		// diff[] -= medianColorChans[];
		// auto colorDist = sqr(diff[0]) + sqr(diff[1]) + sqr(diff[2]);
		auto colorCount = colors[pipColor];
		auto colorFreq = float(colorCount) / numFaceCoords;

		version(unittest)
		{
			writef("%1.5f %s", colorFreq, pipColor);
			if (pip % 3 == 2)
				writeln;
			else
				write(" ");
			if (pip == 8)
				writeln;
		}

		bool isPip;
		// if (colorFreq < 0.08)
		// 	isPip = true;
		// else
		// if (colorFreq > 0.18)
		// 	isPip = false;
		// else
		// 	throw new Exception("Unrecognized pip %d with color %s / freq %s at %s,%s"
		// 		.format(pip, pipColor, colorFreq, dieX + pipX, dieY + pipY));
		isPip = abs(int(pipColor) - int(maxColor)) >= 4;

		pips |= isPip << pip;
	}

	switch (pips)
	{
		case 0b000010000: return 0;
		case 0b001000100: return 1;
		case 0b100010001: return 2;
		case 0b101000101: return 3;
		case 0b101010101: return 4;
		case 0b111000111: return 5;
		default: throw new Exception("Unknown pips on die: %09b".format(pips));
	}
}

unittest
{
	import std.file, std.path;

	foreach (de; dirEntries("die-tests", "*.bmp", SpanMode.shallow))
	{
		scope(failure) writeln("Error with ", de.name);
		auto face = de.read.parseBMP!BGRA;
		auto result = readDie(0, 0, face);
		auto expected = [de.baseName[0]].to!int - 1;
		assert(result == expected, format("Wrong die result (expected %s, got %s)", expected, result));
	}
}

ubyte readDie(int dieX, int dieY)
{
	static Image!BGRA face;
	captureRect(Rect!int(
		screenX0 + dieX - dieSize/2,
		screenY0 + dieY - dieSize/2,
		screenX0 + dieX + dieSize/2,
		screenY0 + dieY + dieSize/2,
	), face);
	scope(failure) face.toBMP.toFile(format("bad-die-%s.bmp", Clock.currTime.stdTime));

	return readDie(dieX, dieY, face);
}

ubyte[5][2] readDice()
{
	static immutable int[2][5][2] dieCoords =
	[
		[
			[1137, 1321],
			[1309, 1446],
			[1504, 1537],
			[1723, 1593],
			[1946, 1616],
		], [
			[2088,  479],
			[2298,  500],
			[2518,  541],
			[2720,  634],
			[2903,  748],
		]
	];

	ubyte[5][2] result;
	foreach (player; 0..2)
		foreach (die; 0..5)
			result[player][die] = readDie(dieCoords[player][die][0], dieCoords[player][die][1]);

	return result;
}

enum pixelCanTalk    = AliasSeq!(1920, 1080, RGB( 24,  22,  11));
enum pixelTalkPoker1 = AliasSeq!(1040, 2020, RGB(206, 162, 123));
enum pixelTalkPoker2 = AliasSeq!(1200, 2020, RGB(210, 166, 128));
enum pixelRollDice   = AliasSeq!(3650, 2050, RGB(232, 238, 225));
enum pixelRaise      = AliasSeq!(1920, 1080, RGB( 27,  33,  20));
enum pixelEndRound   = AliasSeq!(1920, 1000, RGB(204, 147,  33));
enum pixelInDice     = AliasSeq!(  64,   64, RGB(164,  54,   8));
enum pixelYouWin     = AliasSeq!(2000,  648, RGB(157, 157, 157));
enum pixelYouLose    = AliasSeq!(2000,  648, RGB(179, 179, 179));
enum pixelMainMenu   = AliasSeq!(1920,  400, RGB( 89, 105,  77));
enum pixelTalkScreen = AliasSeq!( 500, 2000, RGB( 21,  27,  19));
enum pixelInitialBet = AliasSeq!(1650, 1041, RGB( 54,  54,   5));
enum pixelCantBet2   = AliasSeq!(1910, 1041, RGB( 35,  35,  35));
enum pixelCantBet3   = AliasSeq!(2170, 1041, RGB( 28,  27,  28));
enum pixelCantRaise2 = AliasSeq!(1910, 1250, RGB( 35,  35,  35));
enum pixelCantRaise3 = AliasSeq!(2170, 1250, RGB( 27,  26,  27));

void doFocusGame()
{
	setMousePos(1920, 1080);
	Thread.sleep(50.msecs);
	click();
}

void doLoad()
{
	writeln("Quick-loading...");
	press("F9");
	Thread.sleep(500.msecs);
	press("Return");
}

void doReset()
{
retry:
	if (checkForPixel(pixelInDice))
	{
		writeln("Still in dice game... attempting to exit");
		press("Escape");
		setMousePos(3783-1920, 1131);
		Thread.sleep(500.msecs);
		click();
		Thread.sleep(500.msecs);
		goto retry;
	}

	if (checkForPixel(pixelMainMenu))
	{
		writeln("Somehow in main menu... attempting to exit");
		press("Escape");
		Thread.sleep(500.msecs);
		goto retry;
	}

	if (checkForPixel(pixelTalkScreen))
	{
		writeln("Somehow in talk screen... attempting to exit");
		foreach_reverse (n; 1..10)
			press(text(n));
		Thread.sleep(5000.msecs);
		goto retry;
	}

	doLoad();
}

void doTalk()
{
	waitForPixel(pixelCanTalk);
	waitUntil(!checkForPixel(pixelCanTalk), click(), Duration.zero);
}

void doTalkPoker()
{
	setMousePos(1920, 1080);
	waitFor(checkForPixel(pixelTalkPoker1) || checkForPixel(pixelTalkPoker2), click(), Duration.zero);
	if (checkForPixel(pixelTalkPoker1)) setMousePos(pixelTalkPoker1[0], pixelTalkPoker1[1]); else
	if (checkForPixel(pixelTalkPoker2)) setMousePos(pixelTalkPoker2[0], pixelTalkPoker2[1]); else
		throw new Exception("Can't find poker button");
	click();
}

void doInitialBet(bool maxBet)
{
	waitForPixel(pixelInitialBet);
	if (maxBet && !checkForPixel(pixelCantBet3))
		setMousePos(pixelCantBet3[0..2]);
	else
	if (maxBet && !checkForPixel(pixelCantBet2))
		setMousePos(pixelCantBet2[0..2]);
	else
		setMousePos(pixelInitialBet[0..2]);
	click();
}

void doRollDice()
{
	setMousePos(3650, 2050);
	waitForPixel(pixelRollDice);
	click();
}

void doWaitSettle()
{
	// work around bug? camera otherwise not focusing correctly when entering game directly without dialog option
	foreach_reverse (x; 0..10)
	{
		Thread.sleep(50.msecs);
		setMousePos(1920 + x, 0);
	}
	Thread.sleep(5.seconds);
	waitForPixel(pixelRaise);
	Thread.sleep(500.msecs); // dice might be still rotated for a frame
}

bool doWaitRoundEnd()
{
	waitForPixel(pixelEndRound);
	if (checkForPixel(pixelYouWin))
		return true;
	else
	if (checkForPixel(pixelYouLose))
		return false;
	else
		throw new Exception("Neither won nor lost?");
}

// return false if opponent passes
bool doRaise(bool maxBet)
{
	do
	{
		if (maxBet && !checkForPixel(pixelCantRaise3)) setMousePos(pixelCantRaise3[0..2]); else
		if (maxBet && !checkForPixel(pixelCantRaise2)) setMousePos(pixelCantRaise2[0..2]); else
			setMousePos(1660, 1250);

		click();
		Thread.sleep(16.msecs);
		if (checkForPixel(pixelEndRound))
			return false;
	} while (checkForPixel(pixelRaise));
	return true;
}

void doReroll(uint solution)
{
	if (!solution)
	{
		writeln("Nothing to reroll.");
		return;
	}

	static immutable int[2][5] rerollCoords =
	[
		[3111-1920, 1625],
		[3454-1920, 1666],
		[3814-1920, 1646],
		[4171-1920, 1559],
		[4491-1920, 1435],
	];

	bool first = true;
	foreach (d; 0..numDice)
		if (solution & (1 << d))
		{
			writefln("Rerolling die %d", 1+d);
			setMousePos(rerollCoords[d][0], rerollCoords[d][1]);
			Thread.sleep(first ? 2500.msecs : 500.msecs);
			click();
			first = false;
		}
}

void doNextRound()
{
	setMousePos(1890, 1450);
	click();
	waitFor(!checkForPixel(pixelEndRound) && !checkForPixel(pixelRaise), {}());
}

void doPlayGame(bool quick)
{
	doInitialBet(true);

	int round;
	int wins, losses;
	do
	{
		writefln("== Round %d", ++round);
		enforce(round <= 10, "Too many rounds!?");

		if (round > 1 && !quick)
		{
			setMousePos(1920, 0);
			Thread.sleep(5000.msecs);
			auto dice = readDice();
			writeln("Old roll: ", dice);
		}

		doRollDice();
		doWaitSettle();

		auto dice = readDice();
		writeln("Roll 1: ", dice);

		auto result = search2(encode(dice[0]), encode(dice[1]));
		foreach (d; 0..numDice)
			write((result.solution & (1 << d)) ? 'R' : '.');
		writefln(" - %5.1f%% chance of winning", real(result.score) / scoreMult * 100);

		if (doRaise(wins > losses || (wins == losses && result.score > scoreMult / 2)))
		{
			doReroll(result.solution);

			doRollDice();
			bool won = doWaitRoundEnd();
			writeln(won ? "We won!" : "We lost!");
			(won ? wins : losses)++;
			// doWaitSettle();
		}
		else
		{
			writeln("Opponent passed (we won).");
			wins++;
		}
		doNextRound();
	}
	while (checkForPixel(pixelInDice));
}

void doPlayLoop()
{
	doFocusGame();
	while (true)
		try
		{
			writefln("##### New game #####");
			doReset();
			doTalk();
			doTalkPoker();
			doPlayGame(false);
		}
		catch (Throwable e)
			writeln(e);
}

struct Dispatch
{
static:
	@("Run quickload/play loop to gather statistics.") void loop() { doPlayLoop(); }
	@("OCR the dice on-screen.")                       void read() { writeln(readDice); }
	@("Play one game.")                                void play() { doPlayGame(true); }
}

version (main_play)
void main(string[] args)
{
	stdout.setvbuf(0, _IONBF);
	funoptDispatch!Dispatch(args);
}
