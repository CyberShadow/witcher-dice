import std.algorithm.iteration;
import std.array;
import std.conv;
import std.exception;
import std.file;
import std.stdio;
import std.string;

enum numDice = 5;
enum diceSides = 6;

immutable int[diceSides^^numDice] diceRanks;

shared static this()
{
	foreach (int rank, hands; readText("emu/hands.txt").splitLines)
	{
		foreach (hand; hands.split)
			diceRanks[hand.to!int] = rank;
	}
}

ubyte[5] decode(uint n)
{
	ubyte[5] dice;
	foreach (i; 0..numDice)
	{
		dice[i] = n % diceSides;
		n /= diceSides;
	}
	return dice;
}

uint encode(ubyte[5] dice)
{
	uint n;
	foreach_reverse (i; 0..numDice)
	{
		assert(dice[i] < 6);
		n = n * diceSides + dice[i];
	}
	return n;
}

unittest
{
	assert(encode(decode(1234)) == 1234);
}

struct SearchSolution
{
	int score = int.min;
	uint solution;
}
enum scoreMult = 1000; // permilles

SearchSolution search1(uint myHand, uint oppRank)
{
	ubyte[5] myDice = decode(myHand);

	// auto oppHand = encode(opponent);
	// auto oppRank = diceRanks[oppHand];

	SearchSolution best;
	foreach (solution; 0..(1 << numDice))
	{
		// uint[numDice] changeIndex;
		uint numChanges;
		foreach (d; 0..numDice)
			if (solution & (1 << d))
				// changeIndex[numChanges++] = d;
				numChanges++;

		auto numRolls = diceSides^^numChanges;
		uint numWins, numDraws;
		foreach (roll; 0..numRolls)
		{
			auto newDice = myDice;
			{
				auto n = roll;
				foreach (d; 0..numDice)
					if (solution & (1 << d))
					{
						newDice[d] = n % diceSides;
						n /= diceSides;
					}
			}
			auto newHand = encode(newDice);
			auto newRank = diceRanks[newHand];
			if (newRank == oppRank)
				numDraws++;
			else
			if (newRank > oppRank)
				numWins++;
		}

		int score = (scoreMult * numWins + scoreMult * numDraws / 2) / numRolls;
		if (best.score < score)
		{
			best.score = score;
			best.solution = solution;
		}
	}
	return best;
}

SearchSolution search1Cached(uint myHand, uint oppRank)
{
	static SearchSolution[][diceSides^^numDice] cache;
	if (!cache[myHand])
		cache[myHand] = new SearchSolution[diceRanks.length];
	auto cell = &cache[myHand][oppRank];
	if (cell.score == int.min)
		*cell = search1(myHand, oppRank);
	return *cell;
}

SearchSolution search2(uint myHand, uint oppHand)
{
	ubyte[5] myDice = decode(myHand);

	SearchSolution best;
	foreach (solution; 0..(1 << numDice))
	{
		// uint[numDice] changeIndex;
		uint numChanges;
		foreach (d; 0..numDice)
			if (solution & (1 << d))
				// changeIndex[numChanges++] = d;
				numChanges++;

		auto numRolls = diceSides^^numChanges;
		uint scoreSum;

		foreach (roll; 0..numRolls)
		{
			auto newDice = myDice;
			{
				auto n = roll;
				foreach (d; 0..numDice)
					if (solution & (1 << d))
					{
						newDice[d] = n % diceSides;
						n /= diceSides;
					}
			}

			auto newHand = encode(newDice);
			auto newRank = diceRanks[newHand];

			auto aiResult = search1Cached(oppHand, newRank);
			scoreSum += scoreMult - aiResult.score;
		}

		int score = scoreSum / numRolls;
		if (best.score < score)
		{
			best.score = score;
			best.solution = solution;
		}
	}
	return best;
}

version (main_dice)
void main()
{
	while (true)
		try
		{
			write("> "); stdout.flush;
			auto s = readln().strip;
			if (s is null) return;
			enforce(s.length == 11 && s[5] == ' ', "Bad format");
			ubyte[5] plHand = s[0.. 5].map!(c => cast(ubyte)(c - '1')).array[0..5];
			ubyte[5] aiHand = s[6..11].map!(c => cast(ubyte)(c - '1')).array[0..5];
			auto result = search2(encode(plHand), encode(aiHand));
			foreach (d; 0..numDice)
				write((result.solution & (1 << d)) ? 'R' : '.');
			writefln(" - %5.1f%% chance of winning", real(result.score) / scoreMult * 100);
		}
		catch (Exception e)
			stderr.writeln(e.msg);
}
